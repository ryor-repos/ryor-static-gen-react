'use strict'

module.exports = {
  description: 'Checks source and runnables code for errors with ESLint',
  usage: () => require('../utils/usage').composeUsageInformation([
    ['-f  --fix', 'Fix errors that can be handled automatically by ESlist'],
    ['-q  --quiet', 'No output unless errors are encountered by ESLint']
  ]),
  run: args => {
    const { fix, quiet } = require('minimist')(args, {
      alias: { f: 'fix', q: 'quiet' },
      boolean: ['f', 'fix', 'q', 'quiet']
    })

    return `eslint${fix ? ' -f' : ''}${quiet ? ' -q' : ''}`
  }
}
