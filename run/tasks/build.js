'use strict'

module.exports = {
  description: 'Runs complete build of site',
  usage: () => require('../utils/usage').composeUsageInformation([
    ['-q  --quiet', 'No output unless errors are encountered by tools']
  ]),
  run: args => {
    const { quiet } = require('minimist')(args, {
      alias: { q: 'quiet' },
      boolean: ['q', 'quiet']
    })
    const sequence = [
      'shx rm -rf build output',
      [
        `tsc${quiet ? ' -q' : ''}`,
        `sass${quiet ? ' -q' : ''}`
      ],
      `rollup${quiet ? ' -q' : ''}`,
      `html${quiet ? ' -q' : ''}`,
      'shx mkdir build',
      'shx mv output/build/index.html build',
      'shx rm -rf output'
    ]

    if (!quiet) sequence.push('log -s Build complete')

    return sequence
  }
}
