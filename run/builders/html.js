'use strict'

const VENDOR_SCRIPT_PATHS = [
  'react/umd/react.[REACT_SCRIPT_SUFFIX].js',
  'react-dom/umd/react-dom.[REACT_SCRIPT_SUFFIX].js'
]

const description = 'Builds site HTML file'

function usage () {
  return require('../utils/usage').composeUsageInformation([
    ['-d  --development', 'Uses development settings'],
    ['-q  --quiet', 'No output unless errors are encountered']
  ])
}

function run (args) {
  const { development, quiet } = require('minimist')(args, {
    alias: { d: 'development', q: 'quiet' },
    boolean: ['d', 'development', 'q', 'quiet']
  })
  const sequence = []

  if (!quiet) sequence.push('log -w "Building HTML file"')

  sequence.push(() => {
    const { existsSync, mkdirSync, readFileSync, writeFileSync } = require('fs')
    const { resolve } = require('path')
    const rootDirectoryPath = resolve(__dirname, '../..')
    const buildDirectoryPath = resolve(rootDirectoryPath, 'output/build')
    const sourceDirectoryPath = resolve(rootDirectoryPath, 'source')
    let html = readFileSync(resolve(sourceDirectoryPath, 'index.html')).toString()

    if (!existsSync(buildDirectoryPath)) mkdirSync(buildDirectoryPath)

    if (development) {
      writeFileSync(
        resolve(buildDirectoryPath, 'index.html'),
        html.replace('</head>', '  <link rel="stylesheet" href="/main.css">\n  </head>').replace(
          '</body>',
          `\n    ${
            VENDOR_SCRIPT_PATHS
              .map(path => path.split('/').pop())
              .map(path => path.replace('[REACT_SCRIPT_SUFFIX]', 'development'))
            .concat(['main.js'])
            .map(path => `<script src='/${path.split('/').pop()}'></script>`)
            .join('\n    ')}\n  </body>`
        )
      )
    } else {
      const { JSDOM, VirtualConsole } = require('jsdom')
      const virtualConsole = new VirtualConsole()

      html = html
        .split('</head>')
        .join(`<style>${readFileSync(resolve(buildDirectoryPath, 'main.css')).toString()}</style></head>`)
        .split('</body>')
        .join(`<script>fetch = () => new Promise(resolve => '')</script><script>${
          require('terser').minify(
            VENDOR_SCRIPT_PATHS
              .map((path) => `node_modules/${path.replace('[REACT_SCRIPT_SUFFIX]', 'production.min')}`)
              .reduce((result, path) => result + readFileSync(path).toString(), '') +
            readFileSync(resolve(buildDirectoryPath, 'main.js')).toString(),
            { output: { comments: false } }
          ).code
        }</script></body>`)

      const document = new JSDOM(html, { runScripts: 'dangerously', virtualConsole }).window.document
      const renderCheckTimeLimit = 500
      const renderCheckInterval = 20

      // eslint-disable-next-line no-inner-declarations
      function verifyPageRender (document) {
        let currentTime = 0

        return new Promise((resolve, reject) => {
          const timeout = setInterval(() => {
            if (document.querySelector('.quiz')) {
              clearInterval(timeout)

              return resolve()
            }

            if (currentTime === renderCheckTimeLimit) {
              clearInterval(timeout)

              return reject(new Error('Page render timed out'))
            }

            currentTime += renderCheckInterval
          }, renderCheckInterval)
        })
      }

      verifyPageRender(document)
        .then(() => {
          const scriptElements = document.body.querySelectorAll('script')

          scriptElements.forEach(scriptElement => document.body.removeChild(scriptElement))

          writeFileSync(
            resolve(buildDirectoryPath, 'index.html'),
            require('html-minifier').minify(
              document.documentElement.innerHTML.split('</body>').join(`${scriptElements[1].outerHTML}</body>`),
              {
                collapseWhitespace: true,
                minifyCSS: true
              }
            )
          )
        })
        .catch(({ message }) => {
          console.error(require('chalk').red(message))

          writeFileSync(resolve(buildDirectoryPath, 'index.html'), html)
        })
    }
  })

  return sequence
}

module.exports = { description, usage, run }
