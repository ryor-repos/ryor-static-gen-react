'use strict'

module.exports = {
  description: 'Transpiles and bundles source Sass files into single CSS file',
  usage: () => require('../utils/usage').composeUsageInformation([
    ['-d  --development', 'Uses development settings (no minification)'],
    ['-q  --quiet', 'No output unless errors are encountered by Sass'],
    ['-w  --watch', 'Watches source Sass files and rebuilds CSS after any changes']
  ]),
  run: args => {
    const { development, quiet, watch } = require('minimist')(args, {
      alias: { d: 'development', q: 'quiet', w: 'watch' },
      boolean: ['d', 'development', 'q:', 'quiet', 'w', 'watch']
    })
    const sequence = []

    if (!quiet && !watch) sequence.push('log -w "Bundling CSS"')

    sequence.push(
      'sass source/main.scss output/build/main.css' +
      ` --style ${development ? 'expanded' : 'compressed'}` +
      `${quiet && !watch ? ' --quiet' : ''}` +
      `${watch ? ' --watch' : ''}`
    )

    return sequence
  }
}
