'use strict'

module.exports = {
  description: 'Transpiles TypeScript into JavaScript ES modules',
  usage: () => require('../../utils/usage').composeUsageInformation([
    ['-q  --quiet', 'No output unless errors are encountered by compiler'],
    ['-w  --watch', 'Run compiler in watch mode']
  ]),
  run: args => {
    const { quiet, watch } = require('minimist')(args, {
      alias: { q: 'quiet', w: 'watch' },
      boolean: ['q', 'quiet', 'w', 'watch']
    })
    const sequence = []

    if (!quiet && !watch) sequence.push('log -w "Transpiling TypeScript"')

    sequence.push(
      'tsc -p run/tools/tsc/config.json' +
      `${watch ? ' --watch --preserveWatchOutput true' : ''}`
    )

    return sequence
  }
}
