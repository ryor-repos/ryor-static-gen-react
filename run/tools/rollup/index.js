'use strict'

module.exports = {
  description: 'Bundles ES modules produced by TypeScript into IIFE modules with Rollup',
  usage: () => require('../../utils/usage').composeUsageInformation([
    ['-d  --development ', 'Uses development settings'],
    ['-q  --quiet', 'No output unless errors are encountered by Rollup'],
    ['-w  --watch', 'Watches ES modules and rebundles development IIFE module on changes']
  ]),
  run: args => {
    const { development, quiet, watch } = require('minimist')(args, {
      alias: { d: 'development', q: 'quiet', w: 'watch' },
      boolean: ['d', 'development', 'q', 'quiet', 'w', 'watch']
    })
    const sequence = []

    if (!quiet && !watch) sequence.push('log -w "Bundling JavaScript"')

    sequence.push(
      'rollup' +
      ` -c run/tools/rollup/config.${development ? 'development' : 'production'}.js` +
      `${!watch ? ' --silent' : ''}` +
      `${watch ? ' -w' : ''}`
    )

    return sequence
  }
}
