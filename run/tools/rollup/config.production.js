import babel from 'rollup-plugin-babel'
import config from './config.development'

export default {
  ...config,
  plugins: [
    babel({
      babelrc: false,
      presets: [
        [
          '@babel/preset-env',
          {
            modules: false,
            targets: {
              browsers: require('../browserslist.json')
            }
          }
        ]
      ]
    })
  ]
}
