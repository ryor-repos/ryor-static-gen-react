## React SPA static site generator ryor template

[React](https://reactjs.org/) SPA generator made with [ryor](https://gitlab.com/ryor-/ryor) which uses [TypeScript](https://www.typescriptlang.org/), [Rollup](https://rollupjs.org/guide/en/), [ESLint](https://eslint.org/), [Sass](https://github.com/sass/node-sass), [chokidar](https://github.com/paulmillr/chokidar) for file-watching during development, [jsdom](https://github.com/jsdom/jsdom) for production build HTML pre-rendering and [Tercer](https://terser.org/) and [HTMLMinifier](https://github.com/kangax/html-minifier) for minification/optimization (production build of demo app should be under 45K gzipped).

Trivia question data for demo app courtesy of [Open Trivia Database](https://opentdb.com/).

### Required:

* [Node](https://nodejs.org/) >= 7.6.0

### Get started:

Download or clone this repository (or click the "Deploy to netlify" image below to both fork the repository on Github and set up continuous deployment with [Netlify](https://www.netlify.com/) and then download/clone your own new repository), install dependencies with npm or Yarn and then use any the following commands:

* `node run develop` to start developing

* `node run build` for production build

* `node run help` for CLI usage help

### Deploy:

Configurations for two continuous deployment options are included.

Automatically set up basic Netlify CD by simply clicking the button below:

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/ryor-/ryor-static-gen-react)

The included configuration file ([netlify.toml](https://gitlab.com/ryor-/ryor-static-gen-react/blob/master/netlify.toml)) contains only a build command which will be triggered by pushes of the master branch. The more robust [Gitlab](https://gitlab.com/) CD configuration file ([.gitlab-ci.yml](https://gitlab.com/ryor-/ryor-static-gen-react/blob/master/.gitlab-ci.yml)) will run all tests before building the site and then deploy only tags to Netlify using the CLI (requires that both the NETLIFY_AUTH_TOKEN and NETLIFY_SITE_ID environment variables are set in the CI/CD Settings for the repository beforehand).

## Demo

[https://ryor-static-gen-react.netlify.com/](https://ryor-static-gen-react.netlify.com/)

## TODO

* Jest testing
