import * as React from 'react'

export function Question({onSelect, questionData:{answerIndex, choices, question}, selectedChoiceIndex}:QuestionProps):JSX.Element
{
  return (
    <div className={`question${selectedChoiceIndex !== undefined ? ' question--answered' : ''}`}>
      <div className='question__text'>{question}</div>
      {choices.map((option:string, index:number):JSX.Element =>
        <div {...{
          key: index,
          className:`question__option question__option--${index}${
            selectedChoiceIndex !== undefined
              ? index === answerIndex
                ? ' question__option--correct'
                : index === selectedChoiceIndex
                  ? ' question__option--incorrect'
                  : ''
              : ''
          }`,
          onClick:():void => onSelect(index)
        }}>{option}</div>
      )}
    </div>
  )
}
