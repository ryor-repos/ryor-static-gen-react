import * as React from 'react'

export function Welcome({onButtonClick}:WelcomeProps):JSX.Element
{
  return (
    <div className='welcome'>
      <div className='welcome__heading'>Welcome!</div>
      <div className='welcome__question'>Ready to take the quiz?</div>
      <div {...{className:'welcome__button', onClick:onButtonClick}}>Go!</div>
    </div>
  )
}
