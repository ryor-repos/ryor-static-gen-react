import * as React from 'react'
import {Logo} from './Logo'

export function Header():JSX.Element
{
  return (
    <header className='header'>
      <Logo />
    </header>
  )
}
