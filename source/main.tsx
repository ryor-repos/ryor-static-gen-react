import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {Quiz} from './components/Quiz'

let container:Element|null = document.querySelector('.container')

if (!container)
{
  container = document.body.appendChild(document.createElement('div'))
  container.classList.add('container')
}

document.addEventListener('touchmove', (event:SafariTouchEvent):void => event.preventDefault())

ReactDOM.render(<Quiz />, container)
