/* eslint no-unused-vars: 0 */
export enum QuizStatus {
  Inactive,
  Active,
  Complete
}
